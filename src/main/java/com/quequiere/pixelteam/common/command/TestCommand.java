package com.quequiere.pixelteam.common.command;

import java.util.ArrayList;
import java.util.List;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

public class TestCommand implements ICommand
{

	private List<String> aliases;
	//public static TeamComputerStorage computerStorage;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "test";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "test";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList<String>();
		aliases.add("te");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
	
		
		if(!(sender instanceof EntityPlayerMP))
		{
			return;
		}
		
		//EntityPlayerMP player = (EntityPlayerMP) sender;
			


	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList<String>();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}
}
