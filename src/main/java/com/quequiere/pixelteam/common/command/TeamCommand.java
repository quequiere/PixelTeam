package com.quequiere.pixelteam.common.command;

import java.util.ArrayList;
import java.util.List;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toclient.PacketOpenMainTeamPanel;
import com.quequiere.pixelteam.common.object.Team;
import net.minecraft.command.CommandException;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class TeamCommand implements ICommand
{

	private List<String> aliases;

	@Override
	public int compareTo(ICommand arg0)
	{
		return 0;
	}

	@Override
	public String getCommandName()
	{
		return "team";
	}

	@Override
	public String getCommandUsage(ICommandSender sender)
	{
		return "team";
	}

	@Override
	public List<String> getCommandAliases()
	{
		aliases = new ArrayList<String>();
		aliases.add("t");
		return aliases;
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException
	{
	
		
		if(!(sender instanceof EntityPlayerMP))
		{
			return;
		}
		
		EntityPlayerMP player = (EntityPlayerMP) sender;
		
		String perm = "pixelteam.team";
		
		if(!PixelTeam.hasPermission(player, perm)&&false)
		{
			player.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez besoin de la permission: "+perm));
			return;
		}
		
		Optional<Team> t = PixelTeam.proxyTeam.getPlayerTeam(player.getUniqueID());
		
		if(!t.isPresent())
		{
			PacketService.network.sendTo(new PacketOpenMainTeamPanel(), player);
		}
		else
		{
			PacketService.network.sendTo(new PacketOpenMainTeamPanel(t.get()), player);
		}
		
	


	}

	@Override
	public boolean checkPermission(MinecraftServer server, ICommandSender sender)
	{
		return true;
	}

	@Override
	public List<String> getTabCompletionOptions(MinecraftServer server, ICommandSender sender, String[] args, BlockPos pos)
	{
		return new ArrayList<String>();
	}

	@Override
	public boolean isUsernameIndex(String[] args, int index)
	{
		return false;
	}
}
