package com.quequiere.pixelteam.common.object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Optional;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;

public class Team
{
	protected UUID id;
	protected String name;
	protected ArrayList<Membre> membres = new ArrayList<Membre>();
	protected ArrayList<TeamComputer> computers = new ArrayList<TeamComputer>();
	
	
	public Team(String name,UUID id)
	{
		this.name = name;
		this.id=id;
	}

	public UUID getId()
	{
		return id;
	}

	public String getName()
	{
		return name;
	}

	public List<TeamComputer> getComputers()
	{
		return Collections.unmodifiableList(this.computers);
	}
	
	
	public List<Membre> getMembres()
	{
		return Collections.unmodifiableList(this.membres);
	}
	
	public static Team fromJson(String s)
	{
		Gson gson = new GsonBuilder().create();
		return gson.fromJson(s, Team.class);
	}
	
	public String toJson()
	{
		if(FMLCommonHandler.instance().getSide().equals(Side.SERVER))
		{
			for(Membre m:this.getMembres())
			{
				m.updateName();
			}
		}
		Gson gson = new GsonBuilder().create();
		return gson.toJson(this);
	}
	
	public Optional<Membre> getMembre(UUID id)
	{
		for(Membre m:this.getMembres())
		{
			if(m.getId().equals(id))
			{
				return Optional.of(m);
			}
		}
		return Optional.absent();
	}
	
	public boolean hasMembre(UUID id)
	{
		return this.getMembre(id).isPresent()?true:false;
	}
	
	
	//------------------ SERVER side methods 
	public boolean inviteMembre(EntityPlayerMP source, EntityPlayerMP target)
	{
		return false;
	}
	
	public boolean tryJoinTeam(EntityPlayerMP p)
	{
		return false;
	}

	public boolean removeMembre(EntityPlayerMP sourceP, UUID membreId)
	{
		return false;
	}

	public boolean changeAdjoint(EntityPlayerMP sourceP, UUID membreId, boolean isAdjoint)
	{
		return false;
	}
	
	public boolean changeLeader(EntityPlayerMP sourceP, UUID membreId)
	{
		return false;
	}

	public ArrayList<EntityPlayerMP> getOnlineMembers()
	{
		return null;
	}

	public boolean removeTeam(EntityPlayerMP sourceP)
	{
		return false;
	}

	public Optional<TeamComputer> getTeamComputer(String name)
	{

		for (TeamComputer tc : this.getComputers())
		{
			if (tc.getName().equals(name))
			{
				return Optional.of(tc);
			}
		}

		return Optional.absent();

	}
	
	
	public Optional<TeamComputer> getTeamComputer(UUID id)
	{

		for (TeamComputer tc : this.getComputers())
		{
			if (tc.getId().equals(id))
			{
				return Optional.of(tc);
			}
		}

		return Optional.absent();

	}

	public boolean buyBox(EntityPlayerMP sourceP, int boiteNumber, String computerName)
	{
		return false;
	}

	public boolean buyComputer(EntityPlayerMP sourceP)
	{
		return false;
	}

	public boolean renameComputer(EntityPlayerMP sourceP, UUID computerId, String computerName)
	{
		return false;
	}

	public boolean reparametrerBoite(EntityPlayerMP sourceP, int boiteNumber, UUID computerId, int accessLevel, String boiteName)
	{
		return false;
	}
	
	


	

}
