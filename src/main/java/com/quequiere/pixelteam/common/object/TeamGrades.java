package com.quequiere.pixelteam.common.object;

public enum TeamGrades
{
	membre,
	adjoint,
	leader;
	
	public boolean canRemoveMembre(Membre m)
	{
		if(m.getGrade().equals(leader))
		{
			return false;
		}
		else if(this.equals(leader))
		{
			return true;
		}
		else if(this.equals(adjoint)&&m.equals(membre))
		{
			return true;
		}
		
		return false;
	}
	
	public boolean canAddMember()
	{
		if(this.equals(adjoint)||this.equals(leader))
		{
			return true;
		}
		
		return false;
	}
}
