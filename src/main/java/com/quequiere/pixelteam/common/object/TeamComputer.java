package com.quequiere.pixelteam.common.object;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.UUID;

import com.google.common.base.Optional;
import com.pixelmonmod.pixelmon.config.PixelmonConfig;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.client.gui.GuiEnum;
import com.quequiere.pixelteam.server.storage.pixelmonmod.PCServerTeam;
import com.quequiere.pixelteam.server.storage.pixelmonmod.TeamComputerStorage;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.CompressedStreamTools;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TeamComputer
{
	private transient TeamComputerStorage compStorage = null;
	private transient Team parentTeam;
	private String name;
	private UUID id;
	private HashMap<Integer, TeamBoite> boites = new HashMap<Integer, TeamBoite>();

	public TeamComputer(Team t, String name)
	{
		this.name = name;
		this.parentTeam = t;
		this.id=UUID.randomUUID();

		TeamComputerStorage computerStorage = new TeamComputerStorage(FMLCommonHandler.instance().getMinecraftServerInstance(), this.getParentTeam().getId(), this.getId());
		PixelmonStorage.computerManager.savePlayer(computerStorage);
	}
	
	


	public UUID getId()
	{
		return id;
	}


	


	public void setName(String name)
	{
		this.name = name;
	}




	public boolean createBox(int number)
	{
		if(this.getBoite(number).isPresent())
		{
			return false;
		}
		
		boites.put(number, new TeamBoite());
		
		return true;
	}

	public Optional<TeamBoite> getBoite(int id)
	{
		if (boites.containsKey(id))
		{
			return Optional.of(boites.get(id));
		}
		else
		{
			return Optional.absent();
		}
	}

	public TeamComputerStorage getCompStorage()
	{

		if (this.compStorage == null)
		{

			String path = FMLCommonHandler.instance().getMinecraftServerInstance().worldServers[0].getSaveHandler().getWorldDirectory() + "/pokemon/teamComputer/";

			File saveDirPath = new File(path);
			if (!saveDirPath.exists())
			{
				saveDirPath.mkdirs();
			}

			File playerFile = new File(path + this.getParentTeam().getId() + "/" + this.getId().toString() + ".comp");
			if (playerFile.exists())
			{
				TeamComputerStorage p = new TeamComputerStorage(FMLCommonHandler.instance().getMinecraftServerInstance(), this.getParentTeam().getId(), this.getId());

				try
				{
					p.readFromNBT(CompressedStreamTools.read(new DataInputStream(new FileInputStream(playerFile))));
				}
				catch (IOException arg6)
				{
					if (PixelmonConfig.printErrors)
					{
						System.out.println("Couldn\'t read team computer data file for " + this.getParentTeam().getId().toString());
					}
				}

				this.setCompStorage(p);

			}
			else
			{
				System.out.println("Fatal error, try to load null computer for: " + this.parentTeam.getName() + " for pc: " + this.getName());
				return null;
			}

		}

		return compStorage;
	}

	public boolean openComputer(EntityPlayerMP sourceP)
	{
		Optional<Membre> source = this.getParentTeam().getMembre(sourceP.getUniqueID());

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		PCServerTeam.sendContentsToPlayer(sourceP, this);
		sourceP.openGui(PixelTeam.pixelteam, GuiEnum.pcteam.getIndex().intValue(), sourceP.worldObj, 0, 0, 0);
		return true;

	}

	public void setCompStorage(TeamComputerStorage compStorage)
	{
		this.compStorage = compStorage;
	}

	public Team getParentTeam()
	{
		return parentTeam;
	}

	public String getName()
	{
		if(name==null)
		{
			return "Renommez moi !";
		}
		return name;
	}

}
