package com.quequiere.pixelteam.common.object;

import java.util.UUID;

import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;

public class Membre
{
	private UUID id;
	private TeamGrades grade = TeamGrades.membre; 
	private String cacheName="NotLoadedName";
	
	
	public Membre(UUID id)
	{
		this.id = id;
	}
	

	public UUID getId()
	{
		return id;
	}


	public TeamGrades getGrade()
	{
		return grade;
	}


	public void setGrade(TeamGrades grade)
	{
		this.grade = grade;
	}
	
	@SideOnly(Side.SERVER)
	public void updateName()
	{
		this.cacheName=FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerProfileCache().getProfileByUUID(this.id).getName();
	}


	public String getName()
	{
		return cacheName;
	}
	

	
	
	

}
