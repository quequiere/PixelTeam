package com.quequiere.pixelteam.common.object;

public class TeamBoite
{

	private TeamGrades minimumAccessGrade = TeamGrades.leader;
	private int accessLevel = -1;
	private String name ="NoName";
	
	public boolean canAccess(Membre m)
	{
		return this.canAccessGrade(m);
	}
	
	
	

	public String getName()
	{
		return name;
	}




	public void setName(String name)
	{
		this.name = name;
	}




	public TeamGrades getMinimumAccessGrade()
	{
		return minimumAccessGrade;
	}



	public void setMinimumAccessGrade(TeamGrades minimumAccessGrade)
	{
		this.minimumAccessGrade = minimumAccessGrade;
	}



	public int getAccessLevel()
	{
		return accessLevel;
	}



	public void setAccessLevel(int accessLevel)
	{
		this.accessLevel = accessLevel;
	}



	private boolean canAccessGrade(Membre m)
	{
		TeamGrades target = m.getGrade();
		
		if(minimumAccessGrade.equals(TeamGrades.membre))
		{
			return true;
		}
		else if(minimumAccessGrade.equals(TeamGrades.adjoint))
		{
			if(target.equals(TeamGrades.membre))
			{
				return false;
			}
			else
			{
				return true;
			}
		}
		else if(minimumAccessGrade.equals(TeamGrades.leader)&&target.equals(TeamGrades.leader))
		{
			return true;
		}
		
		return false;
	}
	
	
}
