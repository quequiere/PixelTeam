package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketChangeAdjointTeam implements IMessage
{

	private String uuidMembre;
	private String uuidTeam;
	private boolean isAdjoint;
	
	public PacketChangeAdjointTeam()
	{
	}
	
	public PacketChangeAdjointTeam(Team t,Membre membre,boolean adjoint)
	{
		this.uuidTeam=t.getId().toString();
		this.uuidMembre=membre.getId().toString();
		this.isAdjoint=adjoint;
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuidTeam= ByteBufUtils.readUTF8String(buf);
		this.uuidMembre= ByteBufUtils.readUTF8String(buf);
		this.isAdjoint=buf.readBoolean();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuidTeam);
		ByteBufUtils.writeUTF8String(buf, this.uuidMembre);
		buf.writeBoolean(this.isAdjoint);
	}

	public static class Handler implements IMessageHandler<PacketChangeAdjointTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketChangeAdjointTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuidTeam);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			
			
			if(team.isPresent())
			{
				if(team.get().changeAdjoint(source, UUID.fromString(m.uuidMembre),m.isAdjoint))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez chang� le statut du membre avec succ�s !"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Un probl�me � eu lieu lors de changement de statut du membre."));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
