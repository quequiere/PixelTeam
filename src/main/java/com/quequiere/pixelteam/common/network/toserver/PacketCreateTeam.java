package com.quequiere.pixelteam.common.network.toserver;

import com.quequiere.pixelteam.PixelTeam;
import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketCreateTeam implements IMessage
{

	private String name;
	
	public PacketCreateTeam()
	{
	}
	
	public PacketCreateTeam(String name)
	{
		this.name=name;
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.name= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.name);
	}

	public static class Handler implements IMessageHandler<PacketCreateTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketCreateTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
		
			    
			    if(PixelTeam.proxyTeam.createTeam(m.name,source.getUniqueID()).isPresent())
			    {
			    	source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Team cr��e avec succ�s !"));
			    }
			    else
			    {
			    	source.addChatMessage(new TextComponentString(TextFormatting.RED + "Le processus de cr�ation de team � �chou�."));
			    }
			
			return null;
		}
	}

}
