package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketAcceptInvitation implements IMessage
{

	private String uuid;
	
	public PacketAcceptInvitation()
	{
	}
	
	public PacketAcceptInvitation(UUID id)
	{
		this.uuid=id.toString();
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuid= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuid);
	}

	public static class Handler implements IMessageHandler<PacketAcceptInvitation, IMessage>
	{
		@Override
		public IMessage onMessage(PacketAcceptInvitation m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuid);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			
			if(team.isPresent())
			{
				if(team.get().tryJoinTeam(source))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez bien join la team !"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Une erreur s'est produite pendant le processus de join team."));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
