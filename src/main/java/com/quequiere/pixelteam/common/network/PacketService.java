package com.quequiere.pixelteam.common.network;

import com.quequiere.pixelteam.common.network.toclient.PacketAddInvitation;
import com.quequiere.pixelteam.common.network.toclient.PacketNotificationAchievement;
import com.quequiere.pixelteam.common.network.toclient.PacketOpenMainTeamPanel;
import com.quequiere.pixelteam.common.network.toserver.PacketAcceptInvitation;
import com.quequiere.pixelteam.common.network.toserver.PacketAddMemberTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketBuyBoxTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketBuyNewComputer;
import com.quequiere.pixelteam.common.network.toserver.PacketChangeAdjointTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketChangeLeader;
import com.quequiere.pixelteam.common.network.toserver.PacketCreateTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketOpenComputerTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketParametterComputerBox;
import com.quequiere.pixelteam.common.network.toserver.PacketRemoveMemberTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketRenameComputer;
import com.quequiere.pixelteam.common.network.toserver.pixelmonmod.SwapPokemonTeam;

import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketService
{
	public static SimpleNetworkWrapper network;
	private static int x = -1;

	public static void init()
	{
		PacketService.network = NetworkRegistry.INSTANCE.newSimpleChannel("PixelTeam");

		PacketService.network.registerMessage(PacketOpenMainTeamPanel.Handler.class, PacketOpenMainTeamPanel.class, x++, Side.CLIENT);
		PacketService.network.registerMessage(PacketNotificationAchievement.Handler.class, PacketNotificationAchievement.class, x++, Side.CLIENT);
		PacketService.network.registerMessage(PacketAddInvitation.Handler.class, PacketAddInvitation.class, x++, Side.CLIENT);
		
		PacketService.network.registerMessage(PacketAddMemberTeam.Handler.class, PacketAddMemberTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketAcceptInvitation.Handler.class, PacketAcceptInvitation.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketCreateTeam.Handler.class, PacketCreateTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketRemoveMemberTeam.Handler.class, PacketRemoveMemberTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketChangeAdjointTeam.Handler.class, PacketChangeAdjointTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketChangeLeader.Handler.class, PacketChangeLeader.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketRemoveMemberTeam.Handler.class, PacketRemoveMemberTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(SwapPokemonTeam.Handler.class, SwapPokemonTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketOpenComputerTeam.Handler.class, PacketOpenComputerTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketBuyBoxTeam.Handler.class, PacketBuyBoxTeam.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketBuyNewComputer.Handler.class, PacketBuyNewComputer.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketRenameComputer.Handler.class, PacketRenameComputer.class, x++, Side.SERVER);
		PacketService.network.registerMessage(PacketParametterComputerBox.Handler.class, PacketParametterComputerBox.class, x++, Side.SERVER);
		
		
	}
}
