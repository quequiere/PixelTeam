package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketParametterComputerBox implements IMessage
{

	private UUID teamId;
	private String boxName;
	private int levelAccess =-1;
	private UUID compId;
	private int boxId;
	
	public PacketParametterComputerBox()
	{
	}
	
	

	public PacketParametterComputerBox(UUID teamId, int boxId,String boxName,int levelAccess, UUID compId)
	{
		this.teamId = teamId;
		this.boxName = boxName;
		this.levelAccess=levelAccess;
		this.compId = compId;
		this.boxId=boxId;
	}



	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.teamId= UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.boxName= ByteBufUtils.readUTF8String(buf);
		this.levelAccess=buf.readInt();
		this.compId= UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.boxId=buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.teamId.toString());
		ByteBufUtils.writeUTF8String(buf, this.boxName);
		buf.writeInt(this.levelAccess);
		ByteBufUtils.writeUTF8String(buf, this.compId.toString());
		buf.writeInt(this.boxId);
	}

	public static class Handler implements IMessageHandler<PacketParametterComputerBox, IMessage>
	{
		@Override
		public IMessage onMessage(PacketParametterComputerBox m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(m.teamId);
			
			
			if(team.isPresent())
			{
				if(team.get().reparametrerBoite(source, m.boxId, m.compId, m.levelAccess, m.boxName))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Les nouveaux param�tres de la boites ont �t� appliqu�s !"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Une erreur s'est produite pendant le parametrage de la boite !"));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
