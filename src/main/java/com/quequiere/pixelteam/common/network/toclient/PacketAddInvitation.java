package com.quequiere.pixelteam.common.network.toclient;

import java.util.UUID;

import com.quequiere.pixelteam.client.gui.team.InvitationsTeamGui;
import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketAddInvitation implements IMessage
{

	private String name;
	private UUID id;

	public PacketAddInvitation(String name, UUID id)
	{
		this.name = name;
		this.id = id;
	}

	public PacketAddInvitation()
	{

	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.name = ByteBufUtils.readUTF8String(buf);
		this.id = UUID.fromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.name);
		ByteBufUtils.writeUTF8String(buf, this.id.toString());
	}

	public static class Handler implements IMessageHandler<PacketAddInvitation, IMessage>
	{

		@Override
		public IMessage onMessage(PacketAddInvitation m, MessageContext ctx)
		{
			if(!InvitationsTeamGui.invitations.containsKey(m.name))
			{
				InvitationsTeamGui.invitations.put(m.name, m.id);
			}
			
			return null;
		}
	}

}
