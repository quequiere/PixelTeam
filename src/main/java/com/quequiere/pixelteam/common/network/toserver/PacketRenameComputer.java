package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketRenameComputer implements IMessage
{

	private UUID teamId;
	private String name;
	private UUID compId;
	
	public PacketRenameComputer()
	{
	}
	
	

	public PacketRenameComputer(UUID teamId, String name, UUID compId)
	{
		this.teamId = teamId;
		this.name = name;
		this.compId = compId;
	}



	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.teamId= UUID.fromString(ByteBufUtils.readUTF8String(buf));
		this.name= ByteBufUtils.readUTF8String(buf);
		this.compId= UUID.fromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.teamId.toString());
		ByteBufUtils.writeUTF8String(buf, this.name);
		ByteBufUtils.writeUTF8String(buf, this.compId.toString());
	}

	public static class Handler implements IMessageHandler<PacketRenameComputer, IMessage>
	{
		@Override
		public IMessage onMessage(PacketRenameComputer m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(m.teamId);
			
			
			if(team.isPresent())
			{
				if(team.get().renameComputer(source, m.compId, m.name))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Le nouveau nom du PC a �t� appliqu�."));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Une erreur s'est produite pendant le rename du PC !"));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
