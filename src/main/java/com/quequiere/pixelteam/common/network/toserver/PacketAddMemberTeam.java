package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketAddMemberTeam implements IMessage
{

	private String name;
	private String uuid;
	
	public PacketAddMemberTeam()
	{
	}
	
	public PacketAddMemberTeam(Team t,String name)
	{
		this.uuid=t.getId().toString();
		this.name=name;
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuid= ByteBufUtils.readUTF8String(buf);
		this.name= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuid);
		ByteBufUtils.writeUTF8String(buf, this.name);
	}

	public static class Handler implements IMessageHandler<PacketAddMemberTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketAddMemberTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuid);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			Optional<EntityPlayerMP> targetO = PixelTeam.getPlayer(m.name);
			
			if(!targetO.isPresent())
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce dresseur n'existe pas, ou n'est pas en ligne."));
				return null;
			}
			
			if(team.isPresent())
			{
				if(team.get().inviteMembre(source, targetO.get()))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez envoy� une invitation avec succ�s"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Un probl�me � eu lieu lors de l'envois de l'invitation"));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
