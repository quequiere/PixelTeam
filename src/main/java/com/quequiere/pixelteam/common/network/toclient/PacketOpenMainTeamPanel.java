package com.quequiere.pixelteam.common.network.toclient;

import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.client.gui.GuiEnum;
import com.quequiere.pixelteam.client.gui.team.MenuTeamGui;
import com.quequiere.pixelteam.client.gui.team.TeamMemberListGui;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketOpenMainTeamPanel implements IMessage
{

	private String teamJson;
	private int online;
	private boolean empty = true;
	
	public PacketOpenMainTeamPanel(Team t)
	{
		teamJson=t.toJson();
		online=t.getOnlineMembers().size();
		empty=false;
	}
	
	public PacketOpenMainTeamPanel()
	{
		
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.empty=buf.readBoolean();
		if(!this.empty)
		{
			this.teamJson= ByteBufUtils.readUTF8String(buf);
			this.online=buf.readInt();
		}
		
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeBoolean(this.empty);
		if(!this.empty)
		{
			ByteBufUtils.writeUTF8String(buf, this.teamJson);
			buf.writeInt(online);
		}
		
	}

	public static class Handler implements IMessageHandler<PacketOpenMainTeamPanel, IMessage>
	{

		@Override
		public IMessage onMessage(PacketOpenMainTeamPanel m, MessageContext ctx)
		{
			EntityPlayerSP p = Minecraft.getMinecraft().thePlayer;
			
			if(!m.empty)
			{
				TeamMemberListGui.skinsNet.clear();
				MenuTeamGui.teamCache=Team.fromJson(m.teamJson);
				MenuTeamGui.online=m.online;
				MenuTeamGui.selfMembre=MenuTeamGui.teamCache.getMembre(Minecraft.getMinecraft().thePlayer.getUniqueID()).get();
				p.openGui(PixelTeam.pixelteam, GuiEnum.team.getIndex().intValue(), p.worldObj, 0, 0, 0);
			}
			else
			{
				MenuTeamGui.teamCache=null;
				MenuTeamGui.selfMembre=null;
				p.openGui(PixelTeam.pixelteam, GuiEnum.withoutTeam.getIndex().intValue(), p.worldObj, 0, 0, 0);
				
				

			}
		
			return null;
		}
	}

}
