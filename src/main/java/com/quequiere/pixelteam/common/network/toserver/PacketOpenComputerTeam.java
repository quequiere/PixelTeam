package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.client.gui.pixelmon.GuiPcTeam;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.object.TeamComputer;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketOpenComputerTeam implements IMessage
{

	private String uuid;
	private String computerName;
	
	public PacketOpenComputerTeam()
	{
	}
	
	public PacketOpenComputerTeam(UUID id,String computerName)
	{
		this.uuid=id.toString();
		this.computerName=computerName;
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuid= ByteBufUtils.readUTF8String(buf);
		this.computerName= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuid);
		ByteBufUtils.writeUTF8String(buf, this.computerName);
		GuiPcTeam.computerNameCache=this.computerName;
	}

	public static class Handler implements IMessageHandler<PacketOpenComputerTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketOpenComputerTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuid);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			
			if(team.isPresent())
			{
				
				Optional<TeamComputer> tc = team.get().getTeamComputer(m.computerName);
				
				if(tc.isPresent())
				{
					if(tc.get().openComputer(source))
					{
						source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez ouvert le PC de team."));
					}
					else
					{
						source.addChatMessage(new TextComponentString(TextFormatting.RED + "Une erreur s'est produite pendant le processus d'open pc team."));
					}
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce pc de team n'existe pas: "+m.computerName));
				}
				
				
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
