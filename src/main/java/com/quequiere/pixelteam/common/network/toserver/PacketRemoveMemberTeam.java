package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketRemoveMemberTeam implements IMessage
{

	private String uuidMembre;
	private String uuidTeam;
	
	public PacketRemoveMemberTeam()
	{
	}
	
	public PacketRemoveMemberTeam(Team t,Membre membre)
	{
		this.uuidTeam=t.getId().toString();
		this.uuidMembre=membre.getId().toString();
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuidTeam= ByteBufUtils.readUTF8String(buf);
		this.uuidMembre= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuidTeam);
		ByteBufUtils.writeUTF8String(buf, this.uuidMembre);
	}

	public static class Handler implements IMessageHandler<PacketRemoveMemberTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketRemoveMemberTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuidTeam);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			
			
			if(team.isPresent())
			{
				if(team.get().removeMembre(source, UUID.fromString(m.uuidMembre)))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez supprim� un membre de la team avec succ�s."));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Un probl�me � eu lieu lors de la supression du membre."));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
