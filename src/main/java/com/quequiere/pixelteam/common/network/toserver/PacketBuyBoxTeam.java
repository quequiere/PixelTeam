package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.object.TeamComputer;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketBuyBoxTeam implements IMessage
{

	private String uuid;
	private String computerName;
	private int boxNumber;

	public PacketBuyBoxTeam()
	{
	}

	public PacketBuyBoxTeam(UUID id, String computerName, int boxNumber)
	{
		this.uuid = id.toString();
		this.computerName = computerName;
		this.boxNumber = boxNumber;
	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuid = ByteBufUtils.readUTF8String(buf);
		this.computerName = ByteBufUtils.readUTF8String(buf);
		this.boxNumber = buf.readInt();
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuid);
		ByteBufUtils.writeUTF8String(buf, this.computerName);
		buf.writeInt(this.boxNumber);
	}

	public static class Handler implements IMessageHandler<PacketBuyBoxTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketBuyBoxTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuid);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);

			if (team.isPresent())
			{
				if (team.get().buyBox(source, m.boxNumber, m.computerName))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "L'achat de la boite s'est d�roul�e avec succ�s"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "un probl�me est survenu lors de l'achat de la boite !"));
				}

			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}

			return null;
		}
	}

}
