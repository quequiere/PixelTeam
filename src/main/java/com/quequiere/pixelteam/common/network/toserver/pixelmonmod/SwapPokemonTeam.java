package com.quequiere.pixelteam.common.network.toserver.pixelmonmod;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.object.TeamComputer;
import com.quequiere.pixelteam.server.storage.pixelmonmod.PCServerTeam;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class SwapPokemonTeam implements IMessage
{

	int firstBox;
	int firstPos;
	int secondBox;
	int secondPos;

	private String uuid;
	private String computerName;

	public SwapPokemonTeam()
	{

	}

	public SwapPokemonTeam(int firstBox, int firstPos, int secondBox, int secondPos, UUID id,String computerName)
	{
		this.firstBox = firstBox;
		this.firstPos = firstPos;
		this.secondBox = secondBox;
		this.secondPos = secondPos;
		this.uuid = id.toString();
		this.computerName=computerName;
	}

	public void toBytes(ByteBuf buffer)
	{
		buffer.writeInt(this.firstBox);
		buffer.writeInt(this.firstPos);
		buffer.writeInt(this.secondBox);
		buffer.writeInt(this.secondPos);
		ByteBufUtils.writeUTF8String(buffer, this.uuid);
		ByteBufUtils.writeUTF8String(buffer, this.computerName);
	}

	public void fromBytes(ByteBuf buffer)
	{
		this.firstBox = buffer.readInt();
		this.firstPos = buffer.readInt();
		this.secondBox = buffer.readInt();
		this.secondPos = buffer.readInt();
		this.uuid = ByteBufUtils.readUTF8String(buffer);
		this.computerName = ByteBufUtils.readUTF8String(buffer);
	}

	public static class Handler implements IMessageHandler<SwapPokemonTeam, IMessage>
	{
		public IMessage onMessage(SwapPokemonTeam message, MessageContext ctx)
		{
			ctx.getServerHandler().playerEntity.getServer().addScheduledTask(() -> {

				EntityPlayerMP source = ctx.getServerHandler().playerEntity;
				UUID id = UUID.fromString(message.uuid);
				Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);

				if (team.isPresent())
				{
					
					Optional<TeamComputer> tc = team.get().getTeamComputer(message.computerName);
					
					if(tc.isPresent())
					{
						PCServerTeam.swapPokemon(ctx.getServerHandler().playerEntity, message.firstBox, message.firstPos, message.secondBox, message.secondPos, tc.get().getCompStorage());
						source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Un op�ration sur le PC de team a �t� catalogu�e !"));
						
						if (tc.get().openComputer(source))
						{
							source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "PC actualis�"));
						}
						else
						{
							source.addChatMessage(new TextComponentString(TextFormatting.RED + "Une erreur s'est produite pendant le processus de changement pc team."));
						}
					}
					else
					{
						source.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce pc de team n'existe pas: "+message.computerName));
					}
					
				
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
				}

			});
			return null;
		}
	}
}
