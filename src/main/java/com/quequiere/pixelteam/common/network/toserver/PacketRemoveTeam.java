package com.quequiere.pixelteam.common.network.toserver;

import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Team;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketRemoveTeam implements IMessage
{

	private String uuidTeam;
	
	public PacketRemoveTeam()
	{
	}
	
	public PacketRemoveTeam(Team t)
	{
		this.uuidTeam=t.getId().toString();
	}


	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.uuidTeam= ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.uuidTeam);
	}

	public static class Handler implements IMessageHandler<PacketRemoveTeam, IMessage>
	{
		@Override
		public IMessage onMessage(PacketRemoveTeam m, MessageContext ctx)
		{
			EntityPlayerMP source = ctx.getServerHandler().playerEntity;
			UUID id = UUID.fromString(m.uuidTeam);
			Optional<Team> team = PixelTeam.proxyTeam.getTeamById(id);
			
			if(team.isPresent())
			{
				if(team.get().removeTeam(source))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez supprim� votre team !"));
				}
				else
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Un probl�me � eu lieu lors de la supression de la team."));
				}
			}
			else
			{
				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous avez envoy� un mauvais team id !"));
			}
			
			return null;
		}
	}

}
