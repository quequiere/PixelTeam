package com.quequiere.pixelteam.common.network.toclient;

import com.quequiere.pixelteam.client.gui.tool.AchievementPopper;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketNotificationAchievement implements IMessage
{

	private String tittle;
	private String text;

	public PacketNotificationAchievement(String tittle, String text)
	{
		this.tittle = tittle;
		this.text = text;
	}

	public PacketNotificationAchievement()
	{

	}

	@Override
	public void fromBytes(ByteBuf buf)
	{
		this.tittle = ByteBufUtils.readUTF8String(buf);
		this.text = ByteBufUtils.readUTF8String(buf);
	}

	@Override
	public void toBytes(ByteBuf buf)
	{
		ByteBufUtils.writeUTF8String(buf, this.tittle);
		ByteBufUtils.writeUTF8String(buf, this.text);
	}

	public static class Handler implements IMessageHandler<PacketNotificationAchievement, IMessage>
	{

		@Override
		public IMessage onMessage(PacketNotificationAchievement m, MessageContext ctx)
		{
			AchievementPopper.displayAchievement(m.tittle, m.text);
			return null;
		}
	}

}
