package com.quequiere.pixelteam.common.proxy;

import java.util.UUID;

import com.google.common.base.Optional;
import com.pixelmonmod.pixelmon.gui.GuiHandler;
import com.quequiere.pixelteam.client.gui.GuiEnum;
import com.quequiere.pixelteam.client.gui.pixelmon.GuiPcTeam;
import com.quequiere.pixelteam.client.gui.team.MenuTeamGui;
import com.quequiere.pixelteam.client.gui.team.WithoutTeamMainGui;
import com.quequiere.pixelteam.common.object.Team;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.achievement.GuiAchievement;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.network.IGuiHandler;

public class TeamProxyClient implements IGuiHandler
{
	public GuiHandler guiHandler = new GuiHandler();
	
	
	public void fmlinitEvent()
	{
		Minecraft.getMinecraft().guiAchievement = new GuiAchievement(Minecraft.getMinecraft());
	}

	public void init()
	{

	}

	
	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{

		GuiEnum gui = GuiEnum.getFromOrdinal(ID);
		if (!Minecraft.getMinecraft().isCallingFromMinecraftThread())
		{
			System.out.println("Dodgy gui call from non-main thread for " + gui.toString());
		}

		switch (gui.ordinal()) {
		case 0:
			return new MenuTeamGui();
		case 1:
			return new WithoutTeamMainGui();
		case 2:
			return new GuiPcTeam();

		default:
			return null;

		}
	}

	@Override
	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z)
	{
	
		return null;
	}

	public Optional<Team> getTeamById(String id)
	{
		return null;
	}

	public Optional<Team> getTeamById(UUID id)
	{
		return null;
	}

	public Optional<Team> getPlayerTeam(UUID playerId)
	{
		return null;
	}

	public Optional<Team> createTeam(String name, UUID leader)
	{
		return null;
	}
}
