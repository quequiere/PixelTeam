package com.quequiere.pixelteam.client.gui.tool;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraftforge.fml.client.FMLClientHandler;

public class OpenTool
{
	public static void openLocal(GuiScreen s)
	{
		Minecraft mc = Minecraft.getMinecraft();

		if (FMLClientHandler.instance().getClient().currentScreen == null)
		{
			mc.displayGuiScreen(s);
		}
	}
}
