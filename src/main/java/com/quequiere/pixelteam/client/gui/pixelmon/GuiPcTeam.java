package com.quequiere.pixelteam.client.gui.pixelmon;

import java.io.IOException;

import org.lwjgl.input.Keyboard;

import com.google.common.base.Optional;
import com.pixelmonmod.pixelmon.client.gui.GuiHelper;
import com.pixelmonmod.pixelmon.client.gui.GuiResources;
import com.pixelmonmod.pixelmon.client.gui.pc.GuiPC;
import com.pixelmonmod.pixelmon.comm.PixelmonData;
import com.quequiere.pixelteam.client.gui.team.BoxParametreGui;
import com.quequiere.pixelteam.client.gui.team.MenuTeamGui;
import com.quequiere.pixelteam.client.gui.team.PCMenuTeamGui;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketBuyBoxTeam;
import com.quequiere.pixelteam.common.object.TeamBoite;
import com.quequiere.pixelteam.common.object.TeamComputer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class GuiPcTeam extends GuiPC
{
	public static String computerNameCache = null;

	private static GuiButton buyButton;
	private static GuiButton parametreButton;


	public GuiPcTeam()
	{
		super();
		this.pcClient = new PCClientTeam();
		this.pcClient.unselectAll();

	}

	public void initGui()
	{
		super.initGui();
		parametreButton = new GuiButton(10, 30, this.height / 6, 85, 20, "Param�tres");
		this.buttonList.add(parametreButton);

	}

	@Override
	protected void drawPC(float var1, int var2, int var3)
	{

	
		if (this.computerNameCache.equals("default") || this.computerNameCache.equals("Renommez moi !"))
		{
			this.drawCenteredString(this.mc.fontRendererObj, "Vous devez rennomer ce PC pour l'utiliser", this.width / 2, this.height / 6 - 20, 16777215);
			return;
		}
		

	
		if (buyButton == null)
		{
			buyButton = new GuiButton(10, this.width / 2 - 45, this.height / 6 + 90, 90, 20, "Acheter la boite");
		}

		RenderHelper.disableStandardItemLighting();
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		this.mc.renderEngine.bindTexture(GuiResources.pcPartyBox);
		this.drawTexturedModalRect(this.width / 2 - 91, this.height / 6 + 151, 0, 0, 182, 29);

		Optional<TeamComputer> tc = MenuTeamGui.teamCache.getTeamComputer(computerNameCache);

		if (tc.isPresent())
		{

			Optional<TeamBoite> boiteO = tc.get().getBoite(this.boxNumber);

			if (boiteO.isPresent())
			{

				this.mc.fontRendererObj.drawString("Pc de team " + computerNameCache + ": boite "+boiteO.get().getName(), this.width / 2 - 70, this.height / 6 - 20, 16777215);
				
				if (this.buttonList.contains(buyButton))
				{
					this.buttonList.remove(buyButton);
				}

				if (boiteO.get().canAccess(MenuTeamGui.selfMembre))
				{
					this.mc.renderEngine.bindTexture(GuiResources.pcBox);
					this.drawTexturedModalRect(this.width / 2 - 91, this.height / 6, 0, 0, 182, 141);

					int slot;
					int xPos;
					for (slot = 0; slot < this.pcNumWidth; ++slot)
					{
						for (int pkt = 0; pkt < this.pcNumHeight; ++pkt)
						{
							xPos = pkt * this.pcNumWidth + slot;
							PixelmonData yPos = this.pcClient.getPokemonAtPos(this.boxNumber, xPos);
							if (yPos != null)
							{
								GuiHelper.bindPokemonSprite(yPos, this.mc);
								int xPos1 = this.pcLeft + slot * this.slotWidth;
								int yPos1 = this.pcTop + pkt * this.slotHeight;
								GuiHelper.drawImageQuad((double) (xPos1 + 2), (double) (yPos1 + 3 - (yPos.isGen6Sprite() ? -3 : 0)), 26.0D, 26.0F, 0.0D, 0.0D, 1.0D, 1.0D, 0.0F);
								if (yPos.heldItem != null)
								{
									this.mc.renderEngine.bindTexture(GuiResources.heldItem);
									GuiHelper.drawImageQuad((double) (xPos1 + 22), (double) (yPos1 + 22), 8.0D, 8.0F, 0.0D, 0.0D, 1.0D, 1.0D, 0.0F);
								}

								if (yPos.selected)
								{
									this.mc.renderEngine.bindTexture(GuiResources.pcBox);
									GuiHelper.drawImageQuad((double) xPos1, (double) (yPos1 + 4), (double) this.slotWidth, (float) (this.slotHeight + 1), 0.0D, 0.55859375D, 0.12109375D, 0.68359375D, 0.0F);
								}

								if (yPos.isInRanch)
								{
									this.mc.renderEngine.bindTexture(GuiResources.padlock);
									GuiHelper.drawImageQuad((double) (xPos1 + 2), (double) (yPos1 + 22), 8.0D, 8.0F, 0.0D, 0.0D, 1.0D, 1.0D, 0.0F);
								}
							}
						}
					}

					for (slot = 0; slot < this.getPartySize(); ++slot)
					{
						PixelmonData arg10 = this.getPartyPokemon(slot);
						if (arg10 != null)
						{
							xPos = this.partyLeft + slot * this.slotWidth;
							int arg11 = this.partyTop;
							GuiHelper.bindPokemonSprite(arg10, this.mc);
							GuiHelper.drawImageQuad((double) (xPos + 2), (double) (arg11 + 3 - (arg10.getNationalPokedexNumber() <= 649 ? 0 : -3)), 26.0D, 26.0F, 0.0D, 0.0D, 1.0D, 1.0D, 0.0F);
							if (arg10.heldItem != null)
							{
								this.mc.renderEngine.bindTexture(GuiResources.heldItem);
								GuiHelper.drawImageQuad((double) (xPos + 18), (double) (arg11 + 22), 8.0D, 8.0F, 0.0D, 0.0D, 1.0D, 1.0D, 0.0F);
							}

							if (arg10.selected)
							{
								this.mc.renderEngine.bindTexture(GuiResources.pcBox);
								GuiHelper.drawImageQuad((double) xPos, (double) (arg11 + 3), (double) this.slotWidth, (float) (this.slotHeight + 1), 0.0D, 0.55859375D, 0.12109375D, 0.68359375D, 0.0F);
							}
						}
					}

					PixelmonData arg9 = this.getSlotAt(var2, var3);
					GuiHelper.drawPokemonInfoPC(var2, var3, arg9, this.zLevel);
				
					if (this.pixelmonMenuOpen)
					{
						this.drawButtonContainer();
					}
				}
				else
				{
					this.drawCenteredString(this.mc.fontRendererObj, "Vous n'avez pas le grade suffisant pour acc�der � cette boite !", this.width / 2, this.height / 6 - 20, 16777215);
				}

			}
			else
			{
				
				if (!this.buttonList.contains(buyButton))
				{
					this.buttonList.add(buyButton);
				}
				this.drawCenteredString(this.mc.fontRendererObj, "Boite n� " + (this.boxNumber + 1) + " : cette boite n'a pas encore �t� achet�e.", this.width / 2, this.height / 6 - 20, 16777215);

			}

		}
		else
		{
			this.drawCenteredString(this.mc.fontRendererObj, "Ce pc de team n'existe pas !", this.width / 2, this.height / 6 - 20, 16777215);
		}

	}

	public void actionPerformed(GuiButton button) throws IOException
	{
		super.actionPerformed(button);

		if (button.equals(buyButton))
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketBuyBoxTeam(MenuTeamGui.teamCache.getId(), computerNameCache, this.boxNumber));
		}
		else if (button.equals(parametreButton))
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new BoxParametreGui(computerNameCache,this.boxNumber));
		}

	}

	
}
