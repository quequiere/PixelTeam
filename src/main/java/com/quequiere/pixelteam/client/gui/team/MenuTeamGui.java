package com.quequiere.pixelteam.client.gui.team;

import java.awt.Color;

import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.pixelmon.GuiPcTeam;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketOpenComputerTeam;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.Team;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class MenuTeamGui extends GuiScreen
{
	public static Team teamCache = null;
	public static Membre selfMembre = null;
	public static int online=0;
	
	private int mousebug = 0;
	
	@Override
	public void initGui()
	{
		
		this.buttonList.clear();
		this.buttonList.add(new GuiButton(0, width/2-35-50, height/2, 70, 20, "Membres") );
		this.buttonList.add(new GuiButton(1, width/2-35+50, height/2, 70, 20, "Historique") );
		this.buttonList.add(new GuiButton(2, width/2-35-50, height/2+30, 70, 20, "Banque") );
		this.buttonList.add(new GuiButton(3, width/2-35+50, height/2+30, 70, 20, "Statistiques") );
		this.buttonList.add(new GuiButton(4, width/2-35, height/2+60, 80, 20, "Administration") );
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		
		if(mousebug<40)
		{
			Mouse.setGrabbed(false); 
			mousebug++;
		}
		
		
		GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
		double scale = 0.12;

		
		//--------------------------- Initialisation immages
		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));
		
		
		this.mc.getTextureManager().bindTexture(Images.banner_team);
		ImgTools.myDrawTexturedModalRect(width * scale*1.05, height * scale*1.7, width - (width * scale *7.2), height - (height * scale * 3.5));

		this.mc.getTextureManager().bindTexture(Images.banner_faction);
		ImgTools.myDrawTexturedModalRect(width * scale*6.15, height * scale*1.7, width - (width * scale *7.2), height - (height * scale * 3.5));

		
		//--------------------------- Initialisation ecritures
		
		this.drawCenteredString(this.fontRendererObj, "Membre en ligne: "+online, width / 2, (int) (height * scale) + 50, Color.YELLOW.getRGB());
		
		GL11.glPushMatrix();
		float xp = (float) ((width / 2) * -0.5);
		float zp = (float) (((height * scale) + 10) * -0.48);
		GL11.glScalef(2F, 2F, 0);
		GL11.glTranslatef(xp, zp, 0);
		RenderHelper.enableGUIStandardItemLighting();
		this.drawCenteredString(this.fontRendererObj, teamCache.getName(), width / 2, (int) (height * scale) + 10, Color.YELLOW.getRGB());
		RenderHelper.disableStandardItemLighting();
		GL11.glTranslatef(-xp, -zp, 0);
		GL11.glPopMatrix();
		
	
		
		super.drawScreen(mouseX, mouseY, partialTicks);

	}
	
	protected void actionPerformed(final GuiButton button)
	{
		if(button.id==0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new TeamMemberListGui());
		}
		else if(button.id==2)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new BanqueMainMenuGui());
		}
		else
		{
			//Minecraft.getMinecraft().thePlayer.closeScreen();
		}
	}
}
