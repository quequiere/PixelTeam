package com.quequiere.pixelteam.client.gui.tool.netimage;

import java.awt.image.BufferedImage;
import net.minecraft.client.renderer.texture.DynamicTexture;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.client.FMLClientHandler;

public class ImageNet
{

	private ResourceLocation resourceLocation;
	public ResourceLocation defaultImage;
	public String url;
	public BufferedImage netPic;

	private boolean hasBeenLoaded = false;

	public ImageNet(ResourceLocation defaultImage, String url)
	{
		this.defaultImage = defaultImage;
		this.url = url;
		Thread t = new LoadThread(this);
		t.start();
	}

	public void loadPreview()
	{
		DynamicTexture previewTexture = new DynamicTexture(netPic);
		resourceLocation = FMLClientHandler.instance().getClient().renderEngine.getDynamicTextureLocation("preivew", previewTexture);
	}

	public ResourceLocation getLocation()
	{
		if (this.netPic == null)
		{
			return this.defaultImage;
		}
		else
		{
			if (!this.hasBeenLoaded)
			{
				this.loadPreview();
				this.hasBeenLoaded = true;
			}
			return this.resourceLocation;
		}

	}

}
