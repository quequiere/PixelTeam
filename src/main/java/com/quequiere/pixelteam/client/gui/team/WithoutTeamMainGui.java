package com.quequiere.pixelteam.client.gui.team;

import org.lwjgl.input.Mouse;

import com.quequiere.pixelteam.client.gui.tool.OpenTool;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class WithoutTeamMainGui extends GuiScreen
{
	int mousebug = 0;
	
	@Override
	public void initGui()
	{
		
		this.buttonList.clear();
		this.buttonList.add(new GuiButton(0, width/2-45-50, height/2, 90, 20, "Cr�er une team") );
		this.buttonList.add(new GuiButton(1, width/2-45+50, height/2, 90, 20, "Invitations") );
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		if(mousebug<40)
		{
			Mouse.setGrabbed(false); 
			mousebug++;
		}
		
		this.drawDefaultBackground();
	
		super.drawScreen(mouseX, mouseY, partialTicks);

	}
	
	protected void actionPerformed(final GuiButton button)
	{
		if(button.id==0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new CreateTeamGui());
		
		}
		else if(button.id==1)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new InvitationsTeamGui());
		}
	}
}
