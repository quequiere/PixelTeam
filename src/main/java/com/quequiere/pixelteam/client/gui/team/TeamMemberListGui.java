package com.quequiere.pixelteam.client.gui.team;

import java.awt.Color;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;
import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.element.InvisibleButton;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import com.quequiere.pixelteam.client.gui.tool.netimage.ImageNet;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketAddMemberTeam;
import com.quequiere.pixelteam.common.object.Membre;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.client.renderer.GlStateManager;
import net.minecraft.client.renderer.RenderHelper;

public class TeamMemberListGui extends GuiScreen
{
	public static HashMap<String, ImageNet> skinsNet = new HashMap<String, ImageNet>();
	private int currentPage = 0;
	private GuiTextField addMemberInput;

	public TeamMemberListGui()
	{
		
		for (Membre m : MenuTeamGui.teamCache.getMembres())
		{
			if(!skinsNet.containsKey(m.getName()))
				skinsNet.put(m.getName(), new ImageNet(Images.quequiere, "https://crafatar.com/renders/head/" + m.getName()));
		}

	}

	public void initGui()
	{
		//en cas de resize
		if(this.addMemberInput!=null)
		{
			this.addMemberInput = new GuiTextField(0,this.fontRendererObj, width/2-45, height - 25, 90, 10);
		}
	}
	
	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		Mouse.setGrabbed(false); 
		this.buttonList.clear();

		this.buttonList.add(new GuiButton(0, width / 2 - 25 - 35, (int) (height * 0.75), 50, 20, "<<"));
		this.buttonList.add(new GuiButton(1, width / 2 - 25 + 35, (int) (height * 0.75), 50, 20, ">>"));

		double scale = 0.12;

		// --------------------------- Initialisation immages
		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));

		List<Membre> membres = MenuTeamGui.teamCache.getMembres();
		int sizeListe = membres.size();
		if (sizeListe > 3)
		{
			sizeListe = 3;
		}

		
		
		
		for (int x = 0 + currentPage; x < sizeListe+1 + currentPage; x++)
		{
			GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
			double sizeX = 60;
			double sizeY = 70;

			double xStart = (width / 2 - ((sizeListe + 1) * 33)) + (x - currentPage) * 70;
			double yStart = (height / 2 - 50);

			this.mc.getTextureManager().bindTexture(Images.rectangle);
			ImgTools.myDrawTexturedModalRect(xStart, yStart, sizeX, sizeY);

			try
			{
				InvisibleButton b = new InvisibleButton(x, (int) (xStart), (int) (yStart), (int) sizeX, (int) sizeY, "");
				b.visible = false;
				this.buttonList.add(b);

				if (x < skinsNet.size())
				{
					this.mc.getTextureManager().bindTexture(skinsNet.get(membres.get(x).getName()).getLocation());

					ImgTools.myDrawTexturedModalRect(xStart + 10, yStart + 2, 35, 35);

					this.drawCenteredString(this.fontRendererObj, membres.get(x).getName(), (int) (xStart + sizeX / 2), (int) (yStart + sizeY - 30), Color.YELLOW.getRGB());
					this.drawCenteredString(this.fontRendererObj, membres.get(x).getGrade().name(), (int) (xStart + sizeX / 2), (int) (yStart + sizeY - 20), Color.GRAY.getRGB());
				}
				else
				{
					this.mc.getTextureManager().bindTexture(Images.plus);
					ImgTools.myDrawTexturedModalRect(xStart + 13, yStart + 17, 35, 35);
					b.id = 999;
				}

			}
			catch (IndexOutOfBoundsException e)
			{
				e.printStackTrace();

			}

		}
		
		
		if(this.addMemberInput!=null)
		{
			this.addMemberInput.drawTextBox();
			this.buttonList.add(new GuiButton(-1, width/2+50, height-29, 35, 20, "ok"));
		}

		// --------------------------- Initialisation ecritures

		GL11.glPushMatrix();
		float xp = (float) ((width / 2) * -0.5);
		float zp = (float) (((height * scale) + 10) * -0.48);
		GL11.glScalef(2F, 2F, 0);
		GL11.glTranslatef(xp, zp, 0);
		RenderHelper.enableGUIStandardItemLighting();
		this.drawCenteredString(this.fontRendererObj, "Membres", width / 2, (int) (height * scale) + 10, Color.YELLOW.getRGB());
		RenderHelper.disableStandardItemLighting();
		GL11.glTranslatef(-xp, -zp, 0);
		GL11.glPopMatrix();

		super.drawScreen(mouseX, mouseY, partialTicks);

	}

	protected void actionPerformed(final GuiButton button)
	{
		if (!(button instanceof InvisibleButton))
		{
			if (button.id == 0)
			{
				if (currentPage > 0)
				{
					currentPage--;
				}
			}
			else if (button.id == 1)
			{
				if (currentPage + 3 < skinsNet.size())
				{
					// a verifier si moins de membre
					currentPage++;
				}
			}
			else if(button.id==-1)
			{
				PacketService.network.sendToServer(new PacketAddMemberTeam(MenuTeamGui.teamCache, this.addMemberInput.getText()));
				this.mc.displayGuiScreen((GuiScreen)null);
			}
		}
		else
		{
			if(button.id!=999)
			{
				this.mc.displayGuiScreen((GuiScreen)null);
				MemberGui mg = new MemberGui(MenuTeamGui.teamCache.getMembres().get(button.id));
				OpenTool.openLocal(mg);
			}
			else
			{
				if(this.addMemberInput!=null)
				{
					this.addMemberInput=null;
				}
				else
				{
					this.addMemberInput = new GuiTextField(0,this.fontRendererObj, width/2-45, height - 25, 90, 10);
					addMemberInput.setMaxStringLength(20);
					addMemberInput.setText("");
					this.addMemberInput.setFocused(true);
				}
			
			}
		}

	}
	
	@Override
	public void updateScreen()
	{
		super.updateScreen();
		if(addMemberInput!=null)
		this.addMemberInput.updateCursorCounter();
	}
	
	@Override
	protected void mouseClicked(int x, int y, int btn)
	{
		try
		{
			super.mouseClicked(x, y, btn);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		if(addMemberInput!=null)
		this.addMemberInput.mouseClicked(x, y, btn);
	}
	
	@Override
	public void keyTyped(char c, int code) {
		
		if (code == Keyboard.KEY_ESCAPE)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new MenuTeamGui());
		}
		else
		{
			try
			{
				super.keyTyped(c, code);
			}
			catch (IOException e)
			{
				e.printStackTrace();
			}
			if(addMemberInput!=null)
			this.addMemberInput.textboxKeyTyped(c, code);
		}
	}
}
