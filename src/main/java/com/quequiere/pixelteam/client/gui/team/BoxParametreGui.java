package com.quequiere.pixelteam.client.gui.team;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import com.google.common.base.Optional;
import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketOpenComputerTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketParametterComputerBox;
import com.quequiere.pixelteam.common.network.toserver.PacketRenameComputer;
import com.quequiere.pixelteam.common.object.TeamBoite;
import com.quequiere.pixelteam.common.object.TeamComputer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public class BoxParametreGui extends GuiScreen
{
	private String computerName;
	private int boxId;

	private GuiTextField inputNameComputer;
	private GuiTextField inputNameBox;
	private GuiTextField inputAccessLevel;

	boolean inuputInit = false;

	public BoxParametreGui(String computerName, int boxId)
	{
		this.computerName = computerName;
		this.boxId = boxId;
	}

	public void initGui()
	{

		this.buttonList.clear();
		this.buttonList.add(new GuiButton(0, width / 2 - 45, height / 2 + 60, 90, 20, "Sauvegarder"));
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		double scale = 0.12;

		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));

		Optional<TeamComputer> tc = MenuTeamGui.teamCache.getTeamComputer(this.computerName);

		if (tc.isPresent())
		{

			Optional<TeamBoite> boiteO = tc.get().getBoite(this.boxId);

			if (boiteO.isPresent())
			{
				if (!inuputInit)
				{
					this.inputNameComputer = new GuiTextField(0, this.fontRendererObj, width / 2 + 10, height / 2 - 60, 90, 10);
					inputNameComputer.setMaxStringLength(20);
					inputNameComputer.setText(this.computerName);
					this.inputNameComputer.setFocused(true);

					this.inputNameBox = new GuiTextField(1, this.fontRendererObj, width / 2 + 10, height / 2 - 40, 90, 10);
					inputNameBox.setMaxStringLength(20);
					inputNameBox.setText(boiteO.get().getName());
					this.inputNameBox.setFocused(false);

					this.inputAccessLevel = new GuiTextField(2, this.fontRendererObj, width / 2 + 10, height / 2 - 20, 90, 10);
					inputAccessLevel.setMaxStringLength(2);
					inputAccessLevel.setText("" + boiteO.get().getAccessLevel());
					this.inputAccessLevel.setFocused(false);

					inuputInit = true;
				}

				this.drawCenteredString(this.mc.fontRendererObj, "Nom de la boite:", this.width / 2 - 60, this.height / 2 - 40, 16777215);
				this.drawCenteredString(this.mc.fontRendererObj, "Niveau d'acc�s n�cessaire: ", this.width / 2 - 60, this.height / 2 - 20, 16777215);

				inputNameBox.drawTextBox();
				inputAccessLevel.drawTextBox();
			}
			else
			{
				this.drawCenteredString(this.mc.fontRendererObj, "Cette boite n'est pas encore achet�e !", this.width / 2, this.height / 2, 16777215);
			}

			if (!inuputInit)
			{
				this.inputNameComputer = new GuiTextField(0, this.fontRendererObj, width / 2 + 10, height / 2 - 60, 90, 10);
				inputNameComputer.setMaxStringLength(20);
				inputNameComputer.setText(this.computerName);
				this.inputNameComputer.setFocused(true);
				inuputInit = true;
			}

			this.drawCenteredString(this.mc.fontRendererObj, "Nom du pc:", this.width / 2 - 60, this.height / 2 - 60, 16777215);
			inputNameComputer.drawTextBox();

		}
		else
		{
			this.drawCenteredString(this.mc.fontRendererObj, "Ce pc de team n'existe pas !", this.width / 2, this.height / 2, 16777215);
		}

		super.drawScreen(mouseX, mouseY, partialTicks);

	}

	protected void actionPerformed(final GuiButton button)
	{

		if (button.id == 0)
		{

			Minecraft.getMinecraft().thePlayer.closeScreen();

			Optional<TeamComputer> tc = MenuTeamGui.teamCache.getTeamComputer(this.computerName);

			if (tc.isPresent())
			{
				if (!this.computerName.equals(this.inputNameComputer.getText()))
				{
					PacketService.network.sendToServer(new PacketRenameComputer(MenuTeamGui.teamCache.getId(), this.inputNameComputer.getText(), tc.get().getId()));
				}

				Optional<TeamBoite> boiteO = tc.get().getBoite(this.boxId);

				if (boiteO.isPresent())
				{
					int i = -1;

					try
					{
						i = Integer.parseInt(inputAccessLevel.getText());
						PacketService.network.sendToServer(new PacketParametterComputerBox(MenuTeamGui.teamCache.getId(), this.boxId,this.inputNameBox.getText(), i, tc.get().getId()));
					}
					catch (NumberFormatException e)
					{
						Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString(TextFormatting.RED + "Le leven access doit �tre un chiffre compris entre -1 et 99"));
					}
				}

			}
			else
			{
				Minecraft.getMinecraft().thePlayer.addChatMessage(new TextComponentString(TextFormatting.RED + "Erreur fatale lors de l'envois des packets ! Cela ne devrait pas arriver, contractez un dev"));
			}

		}

	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();

		if (inputNameComputer != null)
		{
			this.inputNameComputer.updateCursorCounter();
		}
		if (inputNameBox != null)
		{
			this.inputNameBox.updateCursorCounter();
		}
		if (inputAccessLevel != null)
		{
			this.inputAccessLevel.updateCursorCounter();
		}

	}

	@Override
	protected void mouseClicked(int x, int y, int btn)
	{
		try
		{
			super.mouseClicked(x, y, btn);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		if (inputNameComputer != null)
		{
			this.inputNameComputer.mouseClicked(x, y, btn);
		}
		if (inputNameBox != null)
		{
			this.inputNameBox.mouseClicked(x, y, btn);
		}
		if (inputAccessLevel != null)
		{
			this.inputAccessLevel.mouseClicked(x, y, btn);
		}

	}

	@Override
	public void keyTyped(char c, int code)
	{

		if (code == Keyboard.KEY_ESCAPE)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketOpenComputerTeam(MenuTeamGui.teamCache.getId(), this.computerName));
			return;
		}

		try
		{
			super.keyTyped(c, code);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		if (inputNameComputer != null)
		{
			if (Character.isLetter(c) || code == Keyboard.KEY_BACK)
			{
				this.inputNameComputer.textboxKeyTyped(c, code);
			}
		}
		if (inputNameBox != null)
		{
			if (Character.isLetter(c) || code == Keyboard.KEY_BACK)
			{
				this.inputNameBox.textboxKeyTyped(c, code);
			}
		}
		if (inputAccessLevel != null)
		{
			this.inputAccessLevel.textboxKeyTyped(c, code);
		}
	}
}
