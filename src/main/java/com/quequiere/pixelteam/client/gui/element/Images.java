package com.quequiere.pixelteam.client.gui.element;

import com.quequiere.pixelteam.PixelTeam;

import net.minecraft.util.ResourceLocation;

public class Images
{
	public static ResourceLocation backGround = new ResourceLocation(PixelTeam.MODID + ":textures/gui/MainInterface.png");
	
	public static ResourceLocation banner_team = new ResourceLocation(PixelTeam.MODID + ":textures/gui/banner_team.png");
	public static ResourceLocation banner_faction = new ResourceLocation(PixelTeam.MODID + ":textures/gui/banner_faction.png");
	public static ResourceLocation quequiere = new ResourceLocation(PixelTeam.MODID + ":textures/gui/quequiere.png");
	public static ResourceLocation rectangle = new ResourceLocation(PixelTeam.MODID + ":textures/gui/rectangle.png");
	public static ResourceLocation plus = new ResourceLocation(PixelTeam.MODID + ":textures/gui/plus.png");

}
