package com.quequiere.pixelteam.client.gui.tool;

import net.minecraft.client.renderer.Tessellator;
import net.minecraft.client.renderer.VertexBuffer;
import net.minecraft.client.renderer.vertex.DefaultVertexFormats;

public class ImgTools {

	 public static void myDrawTexturedModalRect(double x, double y, double width, double height)
	 {
		 /*
	     Tessellator tessellator = Tessellator.instance;
	     
	     tessellator.startDrawingQuads();    
	     tessellator.addVertexWithUV(x        , y + height, 0, 0.0, 1.0);
	     tessellator.addVertexWithUV(x + width, y + height, 0, 1.0, 1.0);
	     tessellator.addVertexWithUV(x + width, y         , 0, 1.0, 0.0);
	     tessellator.addVertexWithUV(x        , y         , 0, 0.0, 0.0);
	     tessellator.draw();*/
		 
		 
		 Tessellator tessellator = Tessellator.getInstance();
	     VertexBuffer vertexbuffer = tessellator.getBuffer();
	     vertexbuffer.begin(7, DefaultVertexFormats.POSITION_TEX);
	     
	     
	     vertexbuffer.pos(x, y + height, 0).tex(0.0, 1.0).endVertex();
	     vertexbuffer.pos(x + width,y + height, 0).tex(1.0, 1.0).endVertex();
	     vertexbuffer.pos(x + width,y, 0).tex(1.0, 0.0).endVertex();
	     vertexbuffer.pos(x, y,0).tex(0.0, 0.0).endVertex();
	     
	     tessellator.draw();
	        
	        
	 }
}
