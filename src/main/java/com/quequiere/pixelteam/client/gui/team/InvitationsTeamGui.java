package com.quequiere.pixelteam.client.gui.team;

import java.awt.Color;
import java.util.HashMap;
import java.util.UUID;

import org.lwjgl.input.Mouse;

import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketAcceptInvitation;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class InvitationsTeamGui extends GuiScreen
{
	public static HashMap<String, UUID> invitations = new HashMap<String, UUID>();
	private int mousebug = 0;
	
	public InvitationsTeamGui()
	{
		
	}
	
	@Override
	public void initGui()
	{
		
		
	
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		if(mousebug<40)
		{
			Mouse.setGrabbed(false); 
			mousebug++;
		} 
		this.drawDefaultBackground();
		this.buttonList.clear();

		if(invitations.size()>0)
		{
			int x = 0;
			for(String name:invitations.keySet())
			{
				this.drawCenteredString(this.fontRendererObj,"Invitation de "+ name, width/2-90, (int) ((height/2-x*22) - (invitations.size()*-5.5)), Color.YELLOW.getRGB());
				this.buttonList.add(new GuiButton(x, width/2,(int) ((height/2-x*22) - (invitations.size()*-5.5))-5, 65, 20, "Accepter") );
				this.buttonList.add(new GuiButton(x, width/2+80,(int) ((height/2-x*22) - (invitations.size()*-5.5))-5, 65, 20, "Refuser") );
				x++;
			}
		}
		else
		{
			this.drawCenteredString(this.fontRendererObj,"Vous n'avez pas d'invitation", width/2, height/2, Color.YELLOW.getRGB());
		}
	
		
		super.drawScreen(mouseX, mouseY, partialTicks);

	}
	
	protected void actionPerformed(final GuiButton button)
	{
		if(button.displayString.equals("Refuser"))
		{
			invitations.remove(invitations.keySet().toArray()[button.id]);
		}
		else if(button.displayString.equals("Accepter"))
		{
			PacketService.network.sendToServer(new PacketAcceptInvitation((UUID) invitations.values().toArray()[button.id]));
			Minecraft.getMinecraft().thePlayer.closeScreen();
			invitations.clear();
		}
	}
}
