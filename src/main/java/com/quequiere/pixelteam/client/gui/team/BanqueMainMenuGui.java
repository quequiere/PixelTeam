package com.quequiere.pixelteam.client.gui.team;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class BanqueMainMenuGui extends GuiScreen
{


	public BanqueMainMenuGui()
	{
	
		
	}
	
	public void initGui()
	{
	
		this.buttonList.clear();
		
		this.buttonList.add(new GuiButton(0, width/2-35-50, height/2, 70, 20, "PC") );
		this.buttonList.add(new GuiButton(1, width/2-35+50, height/2, 70, 20, "Argent") );

	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		double scale = 0.12;
		
		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));
		
		


		super.drawScreen(mouseX, mouseY, partialTicks);
		
		

	}

	protected void actionPerformed(final GuiButton button)
	{

		if(button.id==0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new PCMenuTeamGui());
		}
		else if(button.id==1)
		{
			
		}
	}
	
	@Override
	public void keyTyped(char c, int code) {
		
		if (code == Keyboard.KEY_ESCAPE)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new MenuTeamGui());
		}
	}
}
