package com.quequiere.pixelteam.client.gui;



public enum GuiEnum
{
	team,withoutTeam,pcteam;
	
	public Integer getIndex() {
		return Integer.valueOf(this.ordinal());
	}
	
	public static boolean hasGUI(String name) {
		try {
			return valueOf(name) != null;
		} catch (Exception arg1) {
			return false;
		}
	}

	public static GuiEnum getFromOrdinal(int id) {
		return values()[id];
	}
}
