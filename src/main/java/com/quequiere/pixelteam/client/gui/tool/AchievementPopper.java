package com.quequiere.pixelteam.client.gui.tool;

import java.lang.reflect.Field;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.achievement.GuiAchievement;
import net.minecraft.init.Items;
import net.minecraft.stats.Achievement;

public class AchievementPopper
{

	public static void displayAchievement(String tittle,String text)
	{
		Minecraft.getMinecraft().guiAchievement.displayAchievement(new Achievement("test", "fek ezpp zeee zpezpofkz ozkerpzokp zpoezpe", 1, 10, Items.ARROW, null));

		GuiAchievement achi = Minecraft.getMinecraft().guiAchievement;
		boolean nextTittle = true;

		try
		{
			for (Field f : achi.getClass().getDeclaredFields())
			{

				f.setAccessible(true);
				if (f.getType().equals(String.class))
				{
					if (nextTittle)
					{

						f.set(achi, tittle);

						nextTittle = false;
					}
					else
					{
						f.set(achi, text);
					}

				}

			}

		}
		catch (IllegalArgumentException e)
		{
			e.printStackTrace();
		}
		catch (IllegalAccessException e)
		{
			e.printStackTrace();
		}
	}
}
