package com.quequiere.pixelteam.client.gui.pixelmon;

import com.pixelmonmod.pixelmon.client.ServerStorageDisplay;
import com.pixelmonmod.pixelmon.comm.PixelmonData;
import com.pixelmonmod.pixelmon.storage.PCClient;
import com.quequiere.pixelteam.client.gui.team.MenuTeamGui;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.pixelmonmod.SwapPokemonTeam;

import net.minecraft.client.Minecraft;

public class PCClientTeam extends PCClient
{
	
	@Override
	public void deletePokemon() {
		//PixelmonData p = this.getSelected();
		//PCPos pos = this.getPos(p);
		//this.deletePokemon(pos.box, pos.pos);
		//Pixelmon.network.sendToServer(new TrashPokemon(pos.box, pos.pos));
		System.out.println("This is not dev yet !");
	}
	
	@Override
	public void swapPokemon(int firstBox, int firstPos, int secondBox, int secondPos) {
		PixelmonData pkt1 = this.getPokemonAtPos(firstBox, firstPos);
		PixelmonData pkt2 = this.getPokemonAtPos(secondBox, secondPos);
		this.storePokemonAtPos(pkt1, secondBox, secondPos);
		this.storePokemonAtPos(pkt2, firstBox, firstPos);
		if (ServerStorageDisplay.countNonEgg() >= 1) {
			//Pixelmon.network.sendToServer(new SwapPokemon(firstBox, firstPos, secondBox, secondPos));
			PacketService.network.sendToServer(new SwapPokemonTeam(firstBox, firstPos, secondBox, secondPos,MenuTeamGui.teamCache.getId(),GuiPcTeam.computerNameCache));
			Minecraft.getMinecraft().thePlayer.closeScreen();
		} else {
			this.storePokemonAtPos(pkt1, firstBox, firstPos);
			this.storePokemonAtPos(pkt2, secondBox, secondPos);
		}

	}

}
