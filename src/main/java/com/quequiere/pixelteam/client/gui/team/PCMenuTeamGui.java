package com.quequiere.pixelteam.client.gui.team;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketBuyNewComputer;
import com.quequiere.pixelteam.common.network.toserver.PacketOpenComputerTeam;
import com.quequiere.pixelteam.common.object.TeamComputer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;

public class PCMenuTeamGui extends GuiScreen
{

	public PCMenuTeamGui()
	{

	}

	public void initGui()
	{
		this.buttonList.clear();

		this.buttonList.add(new GuiButton(-1, width / 2 - 70, height / 2 - 55, 140, 20, "Acheter un nouveau PC"));

		int interligne = 13;
		int x = 0;
		for (TeamComputer comp : MenuTeamGui.teamCache.getComputers())
		{
			if (x % 2 == 0)
			{
				this.buttonList.add(new GuiButton(x, width/2-45-50, height/2+x*interligne-25, 90, 20, comp.getName()));
			}
			else
			{
				this.buttonList.add(new GuiButton(x, width/2-45+50, height/2+(x-1)*interligne-25, 90, 20, comp.getName()));
			}
			
			x++;

		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{

		double scale = 0.12;

		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));

	

		super.drawScreen(mouseX, mouseY, partialTicks);

	}

	protected void actionPerformed(final GuiButton button)
	{
		if (button.id >= 0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketOpenComputerTeam(MenuTeamGui.teamCache.getId(), button.displayString));
		}
		else if (button.id == -1)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketBuyNewComputer(MenuTeamGui.teamCache.getId()));
		}
	}

	@Override
	public void keyTyped(char c, int code)
	{

		if (code == Keyboard.KEY_ESCAPE)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new BanqueMainMenuGui());
		}
	}
}
