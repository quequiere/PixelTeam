package com.quequiere.pixelteam.client.gui.team;

import java.awt.Color;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.lwjgl.opengl.GL11;

import com.quequiere.pixelteam.client.gui.element.Images;
import com.quequiere.pixelteam.client.gui.tool.ImgTools;
import com.quequiere.pixelteam.client.gui.tool.OpenTool;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketChangeAdjointTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketChangeLeader;
import com.quequiere.pixelteam.common.network.toserver.PacketRemoveMemberTeam;
import com.quequiere.pixelteam.common.network.toserver.PacketRemoveTeam;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.TeamGrades;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.renderer.RenderHelper;
import net.minecraftforge.fml.client.config.GuiCheckBox;

public class MemberGui extends GuiScreen
{

	private Membre membre;

	public MemberGui(Membre membre)
	{
		this.membre = membre;
		
		
	}
	
	public void initGui()
	{
		double scale = 0.12;
		

		this.buttonList.clear();
		if(Minecraft.getMinecraft().thePlayer.getName().equals(membre.getName()))
		{
			if(!membre.getGrade().equals(TeamGrades.leader))
			{
				this.buttonList.add(new GuiButton(0, width / 2  - 37, (int) (height * 0.78), 75, 20, "Quitter"));
			}
			else
			{
				this.buttonList.add(new GuiButton(4, width / 2  - 37, (int) (height * 0.78), 75, 20, "Supprimer team"));
			}

		}
		else if(MenuTeamGui.selfMembre.getGrade().canRemoveMembre(this.membre))
		{
			this.buttonList.add(new GuiButton(1, width/2-37, (int) (height * 0.78), 75, 20, "Supprimer"));
			
			if(MenuTeamGui.selfMembre.getGrade().equals(TeamGrades.leader))
			{
				this.buttonList.add(new GuiButton(2, (int) ((width *scale)*1.1), (int) (height * 0.78), 90, 20, "Nommer leader"));
				
				this.buttonList.add(new GuiCheckBox(3, (int) (width - (width * scale * 2.2)) ,  (int) (height * 0.80), " Adjoint",membre.getGrade().equals(TeamGrades.adjoint)?true:false));
			}
			
		}
	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{

		Mouse.setGrabbed(false); 
		double scale = 0.12;
		
		this.drawDefaultBackground();

		this.mc.getTextureManager().bindTexture(Images.backGround);
		ImgTools.myDrawTexturedModalRect(width * scale, height * scale, width - (width * scale * 2), height - (height * scale * 2));
		
		

		// --------------------------- Initialisation immages

		
		this.mc.getTextureManager().bindTexture(TeamMemberListGui.skinsNet.get(membre.getName()).getLocation());
		ImgTools.myDrawTexturedModalRect(width/2-25, (height * scale)*2.2, 50, 50);
		
		
	
		
			
		
		
		this.drawCenteredString(this.fontRendererObj, "Grade: ", (int) ((width * scale)*1.5), (int) ((height * scale)*4), Color.YELLOW.getRGB());
		this.drawCenteredString(this.fontRendererObj, this.membre.getGrade().name(), (int) ((width * scale)*1.5)+38, (int) ((height * scale)*4), Color.WHITE.getRGB());

		// --------------------------- Initialisation ecritures

		GL11.glPushMatrix();
		float xp = (float) ((width / 2) * -0.5);
		float zp = (float) (((height * scale) + 10) * -0.48);
		GL11.glScalef(2F, 2F, 0);
		GL11.glTranslatef(xp, zp, 0);
		RenderHelper.enableGUIStandardItemLighting();
		this.drawCenteredString(this.fontRendererObj, this.membre.getName(), width / 2, (int) (height * scale) + 10, Color.YELLOW.getRGB());
		RenderHelper.disableStandardItemLighting();
		GL11.glTranslatef(-xp, -zp, 0);
		GL11.glPopMatrix();

		super.drawScreen(mouseX, mouseY, partialTicks);
		
		

	}

	protected void actionPerformed(final GuiButton button)
	{

		if (button.id == 0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketRemoveMemberTeam(MenuTeamGui.teamCache, membre));
		}
		else if (button.id == 1)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketRemoveMemberTeam(MenuTeamGui.teamCache, membre));
		}
		else if (button.id == 2)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketChangeLeader(MenuTeamGui.teamCache, membre));
		}
		else if (button.id == 3)
		{
			GuiCheckBox cb = (GuiCheckBox) button;
			boolean isAdjoint = cb.isChecked();
			
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketChangeAdjointTeam(MenuTeamGui.teamCache, membre,isAdjoint));
		}
		else if (button.id == 4)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketRemoveTeam(MenuTeamGui.teamCache));
		}

	}
	
	@Override
	public void keyTyped(char c, int code) {
		
		if (code == Keyboard.KEY_ESCAPE)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			OpenTool.openLocal(new TeamMemberListGui());
		}
	}
}
