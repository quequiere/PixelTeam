package com.quequiere.pixelteam.client.gui.tool.netimage;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import javax.imageio.ImageIO;

public class LoadThread extends Thread
{

	public boolean hasFinish = false;
	public BufferedImage img = null;
	public ImageNet imagenet;

	public LoadThread(ImageNet imagenet)
	{
		this.imagenet = imagenet;
	}

	@Override
	public void run()
	{

		URL url;
		try
		{
			url = new URL(this.imagenet.url);
			img = ImageIO.read(url);
			this.imagenet.netPic = img;
		}
		catch (MalformedURLException e)
		{
			e.printStackTrace();

		}
		catch (IOException e)
		{
			e.printStackTrace();

		}

		hasFinish = true;
	}
}
