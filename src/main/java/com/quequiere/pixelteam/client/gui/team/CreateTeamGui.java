package com.quequiere.pixelteam.client.gui.team;

import java.io.IOException;

import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toserver.PacketCreateTeam;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiTextField;

public class CreateTeamGui extends GuiScreen
{
	private GuiTextField teamNameInput;
	
	private int mousebug =0;

	@Override
	public void initGui()
	{

		this.buttonList.clear();
		this.buttonList.add(new GuiButton(0, width / 2 - 45, height / 2, 90, 20, "Cr�er"));

		this.teamNameInput = new GuiTextField(0, this.fontRendererObj, width / 2 - 60, height / 2 - 40, 120, 10);
		teamNameInput.setMaxStringLength(20);
		teamNameInput.setText("");
		this.teamNameInput.setFocused(true);

	}

	@Override
	public void drawScreen(int mouseX, int mouseY, float partialTicks)
	{
		if(mousebug<40)
		{
			Mouse.setGrabbed(false); 
			mousebug++;
		}
		
		this.drawDefaultBackground();

		if (this.teamNameInput != null)
		{
			this.teamNameInput.drawTextBox();
		}
		super.drawScreen(mouseX, mouseY, partialTicks);

	}

	protected void actionPerformed(final GuiButton button)
	{
		if (button.id == 0)
		{
			Minecraft.getMinecraft().thePlayer.closeScreen();
			PacketService.network.sendToServer(new PacketCreateTeam(this.teamNameInput.getText()));
		}
	}

	@Override
	public void updateScreen()
	{
		super.updateScreen();
		if (teamNameInput != null)
			this.teamNameInput.updateCursorCounter();
	}

	@Override
	protected void mouseClicked(int x, int y, int btn)
	{
		try
		{
			super.mouseClicked(x, y, btn);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		if (teamNameInput != null)
			this.teamNameInput.mouseClicked(x, y, btn);
	}

	@Override
	public void keyTyped(char c, int code)
	{

		try
		{
			super.keyTyped(c, code);
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
		if (teamNameInput != null)
		{
			if (Character.isLetter(c) || code == Keyboard.KEY_BACK)
			{
				this.teamNameInput.textboxKeyTyped(c, code);
			}
		}

	}
}
