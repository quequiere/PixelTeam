package com.quequiere.pixelteam.server.proxy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.object.TeamGrades;
import com.quequiere.pixelteam.common.proxy.TeamProxyClient;
import com.quequiere.pixelteam.server.object.TeamServer;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;

public class TeamProxyServer extends TeamProxyClient
{
	public static TeamProxyServer proxy;
	private static HashMap<UUID, TeamServer> teams = new HashMap<UUID, TeamServer>();
	
	

	
	
	
	public List<TeamServer> getTeams()
	{
		return new ArrayList<TeamServer>(teams.values());
	}

	public void addTeam(UUID id, TeamServer ts)
	{
		teams.put(id, ts);
	}


	public void fmlinitEvent()
	{

	}


	public void init()
	{
		proxy = this;
	}

	@Override
	public Optional<Team> getTeamById(String id)
	{
		return getTeamById(UUID.fromString(id));
	}

	@Override
	public Optional<Team> getTeamById(UUID id)
	{
		Team t = teams.get(id);

		if (t == null)
		{
			return Optional.absent();
		}
		else
		{
			return Optional.of(t);
		}

	}

	@Override
	public Optional<Team> getPlayerTeam(UUID playerId)
	{
		for (Team t : this.getTeams())
		{
			if (t.hasMembre(playerId))
			{
				return Optional.of(t);
			}
		}

		return Optional.absent();
	}

	@Override
	public Optional<Team> createTeam(String name, UUID leader)
	{
		Optional<EntityPlayerMP> player = PixelTeam.getPlayer(leader);

		if (this.getPlayerTeam(leader).isPresent())
		{
			return Optional.absent();
		}

		char[] chars = name.toCharArray();

		for (char c : chars)
		{
			if (!Character.isLetter(c))
			{
				if (player.isPresent())
					player.get().addChatMessage(new TextComponentString(TextFormatting.RED + "Caract�re invalide dans le nom de la team !"));
				return Optional.absent();
			}
		}

		for (Team t : this.getTeams())
		{
			if (name.equalsIgnoreCase(t.getName()))
			{
				if (player.isPresent())
					player.get().addChatMessage(new TextComponentString(TextFormatting.RED + "Ce nom de team existe d�j� !"));
				return Optional.absent();
			}
		}

		UUID futurId = UUID.randomUUID();
		while (this.getTeamById(futurId).isPresent())
		{
			futurId = UUID.randomUUID();
		}

		TeamServer ts = new TeamServer(name, futurId);
		this.addTeam(futurId, ts);
		Membre m = new Membre(leader);
		ts.addMembre(m);
		m.setGrade(TeamGrades.leader);
		Team t = ts;

		return Optional.of(t);
	}


	public Object getServerGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return this.guiHandler.getServerGuiElement(ID, player, world, x, y, z);
	}

	public Object getClientGuiElement(int ID, EntityPlayer player, World world, int x, int y, int z) {
		return null;
	}
	


}
