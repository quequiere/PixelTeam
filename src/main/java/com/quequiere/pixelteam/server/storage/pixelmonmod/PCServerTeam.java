package com.quequiere.pixelteam.server.storage.pixelmonmod;

import java.util.Optional;

import com.pixelmonmod.pixelmon.Pixelmon;
import com.pixelmonmod.pixelmon.comm.PixelmonData;
import com.pixelmonmod.pixelmon.comm.packetHandlers.pcClientStorage.PCAdd;
import com.pixelmonmod.pixelmon.comm.packetHandlers.pcClientStorage.PCClear;
import com.pixelmonmod.pixelmon.entities.pixelmon.EntityPixelmon;
import com.pixelmonmod.pixelmon.storage.ComputerBox;
import com.pixelmonmod.pixelmon.storage.PixelmonStorage;
import com.pixelmonmod.pixelmon.storage.PlayerComputerStorage;
import com.pixelmonmod.pixelmon.storage.PlayerStorage;
import com.quequiere.pixelteam.common.object.TeamComputer;

import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;

public class PCServerTeam 
{
	public static void deletePokemon(EntityPlayerMP player, int box, int pos)
	{
		/*
		Optional optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (optstorage.isPresent())
		{
			PlayerStorage storage = (PlayerStorage) optstorage.get();
			storage.recallAllPokemon();
		}

		Pixelmon.EVENT_BUS.post(new PixelmonDeletedEvent(player, getPokemonAtPos(player, box, pos), DeleteType.PC));
		storePokemonAtPos(player, (NBTTagCompound) null, box, pos);*/
		
		System.out.println("Not dev yet !");
	}

	public static void swapPokemon(EntityPlayerMP player, int firstBox, int firstPos, int secondBox, int secondPos,TeamComputerStorage teamStorage)
	{
		NBTTagCompound n1 = getPokemonAtPos(player, firstBox, firstPos,teamStorage);
		NBTTagCompound n2 = getPokemonAtPos(player, secondBox, secondPos,teamStorage);
		unloadEntity(player, firstBox, firstPos);
		unloadEntity(player, secondBox, secondPos);
		storePokemonAtPos(player, n1, secondBox, secondPos,teamStorage);
		storePokemonAtPos(player, n2, firstBox, firstPos,teamStorage);
	}

	private static void storePokemonAtPos(EntityPlayerMP player, NBTTagCompound nbt, int box, int pos,TeamComputerStorage teamStorage)
	{
		if (box == -1)
		{
			Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
			if (optstorage.isPresent())
			{
				PlayerStorage storage = (PlayerStorage) optstorage.get();
				storage.changePokemon(pos, nbt);
			}
		}
		else
		{
			teamStorage.getBox(box).changePokemon(pos, nbt);
			PixelmonStorage.computerManager.savePlayer(teamStorage);
		}

	}
	
	private static void unloadEntity(EntityPlayerMP player, int box, int pos) {
		Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
		if (optstorage.isPresent()) {
			PlayerStorage storage = (PlayerStorage) optstorage.get();
			int[] id = storage.getIDFromPosition(pos);
			if (id[0] != -1 && id[1] != -1) {
				Optional<EntityPixelmon> p = storage.getAlreadyExists(id, player.worldObj);
				if (p.isPresent()) {
					((EntityPixelmon) p.get()).unloadEntity();
				}
			}
		}

	}



	public static NBTTagCompound getPokemonAtPos(EntityPlayerMP player, int box, int pos,TeamComputerStorage teamStorage)
	{
		if (box == -1)
		{
			Optional<PlayerStorage> optstorage = PixelmonStorage.pokeBallManager.getPlayerStorage(player);
			if (optstorage.isPresent())
			{
				PlayerStorage storage = (PlayerStorage) optstorage.get();
				return storage.getList()[pos];
			}
			else
			{
				return null;
			}
		}
		else
		{
			return teamStorage.getBox(box).getNBTByPosition(pos);
		}
	}

	public static void sendContentsToPlayer(EntityPlayerMP player,TeamComputer teamComputer)
	{
		Pixelmon.network.sendTo(new PCClear(), player);
		PlayerComputerStorage s = teamComputer.getCompStorage();
		ComputerBox[] arg1 = s.getBoxList();
		int arg2 = arg1.length;
		

		for (int arg3 = 0; arg3 < arg2; ++arg3)
		{
			ComputerBox b = arg1[arg3];
			NBTTagCompound[] arg5 = b.getStoredPokemon();
			int arg6 = arg5.length;

			for (int arg7 = 0; arg7 < arg6; ++arg7)
			{
				NBTTagCompound n = arg5[arg7];
				if (n != null)
				{
					PixelmonData p = new PixelmonData(n);
					Pixelmon.network.sendTo(new PCAdd(p), player);
				}
			}
		}

	}
}
