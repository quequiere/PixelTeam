package com.quequiere.pixelteam.server.storage.pixelmonmod;

import java.io.File;
import java.util.UUID;

import com.pixelmonmod.pixelmon.storage.PlayerComputerStorage;

import net.minecraft.server.MinecraftServer;

public class TeamComputerStorage extends PlayerComputerStorage
{
	private UUID computerUUID = null;
	
	public TeamComputerStorage(MinecraftServer server, UUID teamUUID,UUID computerUUID)
	{
		super(server, teamUUID);
		
		this.computerUUID=computerUUID;
		
		File f = new File(this.getSaveFolder(server) + "teamComputer/"+teamUUID.toString()+"/");
		
		if (!f.exists()) {
			f.mkdirs();	
		}	
		
		
		this.saveFile = this.getSaveFolder(server) + "teamComputer/"+teamUUID.toString() +"/"+this.computerUUID.toString()+ ".comp";
	}

}
