package com.quequiere.pixelteam.server.object;

import java.util.ArrayList;
import java.util.UUID;

import com.google.common.base.Optional;
import com.quequiere.pixelteam.PixelTeam;
import com.quequiere.pixelteam.common.network.PacketService;
import com.quequiere.pixelteam.common.network.toclient.PacketAddInvitation;
import com.quequiere.pixelteam.common.network.toclient.PacketNotificationAchievement;
import com.quequiere.pixelteam.common.object.Membre;
import com.quequiere.pixelteam.common.object.Team;
import com.quequiere.pixelteam.common.object.TeamBoite;
import com.quequiere.pixelteam.common.object.TeamComputer;
import com.quequiere.pixelteam.common.object.TeamGrades;
import com.quequiere.pixelteam.server.proxy.TeamProxyServer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class TeamServer extends Team
{
	private transient ArrayList<UUID> invitations = new ArrayList<UUID>();

	public TeamServer(String name, UUID id)
	{
		super(name, id);
		this.createComputer("default");
	}

	private void createComputer(String name)
	{
		this.computers.add(new TeamComputer(this, name));
	}

	public void addMembre(Membre m)
	{
		this.membres.add(m);
	}

	public void removeMembre(Membre m)
	{
		this.membres.remove(m);
	}

	public void changeGrade(Membre m, TeamGrades t)
	{
		m.setGrade(t);
	}

	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public ArrayList<EntityPlayerMP> getOnlineMembers()
	{
		ArrayList<EntityPlayerMP> liste = new ArrayList<EntityPlayerMP>();

		for (Membre m : this.getMembres())
		{
			EntityPlayerMP p = FMLCommonHandler.instance().getMinecraftServerInstance().getPlayerList().getPlayerByUUID(m.getId());
			if (p != null)
			{
				liste.add(p);
			}
		}
		return liste;

	}

	@Override
	public boolean changeAdjoint(EntityPlayerMP sourceP, UUID membreId, boolean isAdjoint)
	{
		Optional<Membre> target = this.getMembre(membreId);
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (!target.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce joueur ne fait pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader))
		{
			for (EntityPlayerMP c : this.getOnlineMembers())
			{
				PacketService.network.sendTo(new PacketNotificationAchievement("Changement de grade", target.get().getName()), c);
			}

			if (isAdjoint)
			{
				this.changeGrade(target.get(), TeamGrades.adjoint);
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Le membre est maintenant adjoint !"));
			}
			else
			{
				this.changeGrade(target.get(), TeamGrades.membre);
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Le membre n'est plus adjoint !"));
			}

			return true;
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour faire cela !"));
			return false;
		}
	}

	@Override
	public boolean removeMembre(EntityPlayerMP sourceP, UUID membreId)
	{
		Optional<Membre> target = this.getMembre(membreId);
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (!target.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce joueur ne fait pas parti de cette team !"));
			return false;
		}

		if (target.get().getId().equals(source.get().getId()))
		{
			if (target.get().getGrade().equals(TeamGrades.leader))
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne pouvez pas quitter la team en tant que leader !"));
				return false;
			}
			else
			{
				for (EntityPlayerMP c : this.getOnlineMembers())
				{
					PacketService.network.sendTo(new PacketNotificationAchievement("Membre parti !", target.get().getName()), c);
				}

				this.removeMembre(target.get());
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez quitt� la team !"));
				return true;
			}

		}

		if (source.get().getGrade().canRemoveMembre(target.get()))
		{
			for (EntityPlayerMP c : this.getOnlineMembers())
			{
				PacketService.network.sendTo(new PacketNotificationAchievement("Membre supprim� !", target.get().getName()), c);
			}

			this.removeMembre(target.get());
			sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Suppression effectu�e !"));
			return true;
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour supprimer ce membre."));
			return false;
		}

	}

	@Override
	public boolean tryJoinTeam(EntityPlayerMP p)
	{
		if (invitations.contains(p.getUniqueID()))
		{
			invitations.remove(p.getUniqueID());
			this.addMembre(new Membre(p.getUniqueID()));

			for (EntityPlayerMP target : this.getOnlineMembers())
			{
				PacketService.network.sendTo(new PacketNotificationAchievement("Nouveau membre", p.getName()), target);
			}

			return true;
		}

		return false;
	}

	@Override
	public boolean inviteMembre(EntityPlayerMP source, EntityPlayerMP target)
	{

		Optional<Team> m = TeamProxyServer.proxy.getPlayerTeam(source.getUniqueID());
		if (m.isPresent() && m.get().getId().equals(this.getId()))
		{
			Membre sMembre = this.getMembre(source.getUniqueID()).get();

			if (sMembre.getGrade().canAddMember())
			{
				if (invitations.contains(target.getUniqueID()))
				{
					source.addChatMessage(new TextComponentString(TextFormatting.GREEN + "L'invitation a �t� retir�e !"));
					invitations.remove(target.getUniqueID());
					target.addChatMessage(new TextComponentString(TextFormatting.RED + "L'invitation de la team " + this.getName() + " a �t� retir�e !"));
					return false;
				}

				if (TeamProxyServer.proxy.getPlayerTeam(target.getUniqueID()).isPresent())
				{
					source.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce joueur fait d�j� partie d'une team"));
					return false;
				}

				invitations.add(target.getUniqueID());
				target.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez re�u une invitation � rejoindre la team: " + this.getName()));
				PacketService.network.sendTo(new PacketNotificationAchievement("Invitation de team", TextFormatting.RED + "/t pour plus d'info"), target);
				PacketService.network.sendTo(new PacketAddInvitation(this.getName(), this.getId()), target);
				return true;
			}
			else
			{

				source.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas les droits pour effectuer cette action."));
				return false;
			}
		}

		return false;
	}

	@Override
	public boolean changeLeader(EntityPlayerMP sourceP, UUID membreId)
	{

		Optional<Membre> target = this.getMembre(membreId);
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (!target.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce joueur ne fait pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader))
		{
			for (EntityPlayerMP c : this.getOnlineMembers())
			{
				PacketService.network.sendTo(new PacketNotificationAchievement("Changement de leader", target.get().getName()), c);
			}

			this.changeGrade(target.get(), TeamGrades.leader);
			this.changeGrade(source.get(), TeamGrades.adjoint);
			sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Le membre est maintenant leader !"));

			return true;
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour faire cela !"));
			return false;
		}

	}

	@Override
	public boolean removeTeam(EntityPlayerMP sourceP)
	{
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader))
		{
			for (Membre m : this.getMembres())
			{
				this.removeMembre(m);
			}

			return true;
		}

		return false;
	}

	
	@Override
	public boolean buyComputer(EntityPlayerMP sourceP)
	{
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());
		

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		
		if (source.get().getGrade().equals(TeamGrades.leader) || source.get().getGrade().equals(TeamGrades.adjoint))
		{
			if (!PixelTeam.buy(10000, this))
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Votre team n'a pas assez d'argent pour acheter un pc !"));
				return false;
			}
			else
			{
				this.createComputer(null);
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez achet� un nouveau PC !"));
				return true;
				
			}
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour acheter un PC !"));
			return false;
		}
		
	}
	
	@Override
	public boolean renameComputer(EntityPlayerMP sourceP, UUID computerId, String computerName)
	{
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());
		Optional<TeamComputer> tc = this.getTeamComputer(computerId);

		if (!tc.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce pc de team n'existe pas: " + computerName));
			return false;
		}

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader) || source.get().getGrade().equals(TeamGrades.adjoint))
		{

			if(this.getTeamComputer(computerName).isPresent())
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce nom de pc existe d�j� !"));
				return false;
			}
			else
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez renomm� un pc !"));
				tc.get().setName(computerName);
				return true;
			}
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour rennomer une box !"));
			return false;
		}

	}
	
	
	@Override
	public boolean reparametrerBoite(EntityPlayerMP sourceP, int boiteNumber, UUID computerId,int accessLevel,String boiteName)
	{
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());
		Optional<TeamComputer> tc = this.getTeamComputer(computerId);

		if (!tc.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce pc de team n'existe pas"));
			return false;
		}

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader) || source.get().getGrade().equals(TeamGrades.adjoint))
		{
			
			Optional<TeamBoite> boite = tc.get().getBoite(boiteNumber);
			
			if(!boite.isPresent())
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "La boite "+boiteNumber+" n'existe pas."));
				return false;
			}
			else
			{
				boite.get().setAccessLevel(accessLevel);
				boite.get().setName(boiteName);
				sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Les param�tres ont �t� mis � jour sur le serveur !"));
				return true;
			}
			
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour acheter une box !"));
			return false;
		}

	}
	
	
	@Override
	public boolean buyBox(EntityPlayerMP sourceP, int boiteNumber, String computerName)
	{
		Optional<Membre> source = this.getMembre(sourceP.getUniqueID());
		Optional<TeamComputer> tc = this.getTeamComputer(computerName);

		if (!tc.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Ce pc de team n'existe pas: " + computerName));
			return false;
		}

		if (!source.isPresent())
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous ne faites pas parti de cette team !"));
			return false;
		}

		if (source.get().getGrade().equals(TeamGrades.leader) || source.get().getGrade().equals(TeamGrades.adjoint))
		{
			if (!PixelTeam.buy(10000, this))
			{
				sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Votre team n'a pas assez d'argent pour acheter cette boite."));
				return false;
			}
			else
			{

				if (tc.get().getBoite(boiteNumber).isPresent())
				{
					sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "La boite " + (boiteNumber + 1) + " existe d�j� !"));
					return false;
				}
				else
				{
					if (tc.get().createBox(boiteNumber))
					{
						sourceP.addChatMessage(new TextComponentString(TextFormatting.GREEN + "Vous avez achet� une boite !"));
						return true;
					}
					else
					{
						sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Quelque choes a �chou� lors de l'achat de la boite."));
						return false;
					}

				}

			}
		}
		else
		{
			sourceP.addChatMessage(new TextComponentString(TextFormatting.RED + "Vous n'avez pas assez de droit pour acheter une box !"));
			return false;
		}

	}

}
